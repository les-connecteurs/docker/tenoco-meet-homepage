FROM nginx:1.19.6-alpine

EXPOSE 80

RUN mkdir -p /app

COPY default.conf /etc/nginx/conf.d/default.conf
COPY index.html /app/index.html
